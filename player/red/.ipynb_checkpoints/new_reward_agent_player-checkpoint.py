from drill.api.bp.gear.agent import AgentInterface, AgentStat
from collections import deque, defaultdict
import numpy as np
import math 

from env.env_util import get_type_num, get_weapon_num
from env.env_def import UnitType, RED_AIRPORT_ID, MapInfo, SideType, MissileType
from common.cmd import Command
from common.grid import MapGrid
from player.red.rule_player import RulePlayer
from common.threat_analysis import ThreatAnalysis

ATTR_NAME = {getattr(UnitType, attr): attr for attr in dir(UnitType) if not attr.startswith('_')}


class PlayerConfig:
    MY_UNIT_TYPES = [UnitType.A2A, UnitType.A2G, UnitType.JAM, UnitType.AWACS]
    MY_UNIT_MASK_TYPES = [UnitType.A2G]
    MAX_MY_UNIT_LEN = 44

    EN_UNIT_TYPES = [UnitType.A2A, UnitType.A2G, UnitType.AWACS, UnitType.SHIP, 
                     UnitType.S2A, UnitType.RADAR, UnitType.COMMAND]
    MAX_EN_UNIT_LEN = 40
    MINI_MAP_SIZE = 32
    GLOBAL_MOVE_SIZE = 4

# todo: 临时放在这里，后面AgetnStat优化之后，会移到AgentStat里面。
full_name_dict, side_sets = {}, {'red': PlayerConfig.MY_UNIT_TYPES, 'blue': PlayerConfig.EN_UNIT_TYPES}
side_full_name = {k: dict() for k in side_sets}

# 从 'pos_x' 和 'pos_y' 到具体位置的映射，设置加分区域和减分区域
patrol_point_map = {
    (0, 0): [-250830, 184288, 8000],    #  +2分
    (0, 1): [-295519, -100815, 8000],   #  +2分
    (0, 2): [-250830, 294288, 8000],    #  +1分
    (0, 3): [-140830, 294288, 8000],    #  +1分
    (1, 0): [-140830, 184288, 8000],    #  +1分
    (1, 1): [-295519, -210815, 8000],   #  +1分
    (1, 2): [-185519, -210815, 8000],   #  +1分
    (1, 3): [-185519, -100815, 8000],   #  +1分
    (2, 0): [-250830, 74288, 8000],     #  +1分
    (2, 1): [-295519, 9185, 8000],      #  +1分
    (2, 2): [-50830, 41736, 8000],      #  -1分
    (2, 3): [59170, 41736, 8000],       #  -1分
    (3, 0): [-50830, 239288, 8000],     #  -1分
    (3, 1): [-95519, -155815, 8000],    #  -1分
    (3, 2): [128000, 40000, 8000],      #  -1分
    (3, 3): [-140000, 40000, 8000],     #  -1分
}

# 分为三类
patrol_area_type1 = [(0, 0), (0, 1)]
patrol_area_type2 = [(0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1)]
patrol_area_type3 = [(2, 2), (2, 3), (3, 0), (3, 1), (3, 2), (3, 3)]

class NJ01Stat(AgentStat):
    def __init__(self):
        super().__init__()
        self.__create_name_dict()

    def __create_name_dict(self):
        for each_side, each_set in side_sets.items():
            for unit_type in each_set:
                type_name = ATTR_NAME[unit_type]
                full_name = '_'.join(['info', each_side, type_name])
                setattr(self, full_name, 0)
                side_full_name[each_side][unit_type] = full_name

    def update_step(self, raw_obs, env_step_info, prev_reward):
        super().update_step(raw_obs, env_step_info, prev_reward)
        if env_step_info.player_done or env_step_info.env_done:
            self.__count(raw_obs)

    def __count(self, raw_obs):
        cnt_dict = {}
        for each_side, each_set in side_sets.items():
            for unit in raw_obs[each_side]['units']:
                unit_type = unit['LX']
                if unit_type in each_set:
                    full_name = side_full_name[each_side][unit_type]
                    cnt_dict[full_name] = cnt_dict.get(full_name, 0) + 1
        for full_name, num in cnt_dict.items():
            setattr(self, full_name, num)

    def summarise(self):
        result = super(NJ01Stat, self).summarise()
        print('result', result)
        return result

class NJ01Player(AgentInterface):

    def __init__(self, side, feature_templates,
                 action_type, network_conf=None):
        super().__init__(feature_templates, action_type, network_conf)
        self.side = side
        self.__init_variables()

    @property
    def agent_stat(self) -> AgentStat:
        return self.__agent_stat

    def __init_variables(self):
        self.my_unit_ids = []
        self.en_unit_ids = []
        self.reward_obj = RedReward()
        # map grid for global move
        self.map_grid = MapGrid(
            (MapInfo.X_MIN, MapInfo.Y_MAX), (MapInfo.X_MAX, MapInfo.Y_MIN), 
            PlayerConfig.GLOBAL_MOVE_SIZE, PlayerConfig.GLOBAL_MOVE_SIZE)
        # map grid for minimap
        self.mini_map_grid = MapGrid(
            (MapInfo.X_MIN, MapInfo.Y_MAX), (MapInfo.X_MAX, MapInfo.Y_MIN), 
            PlayerConfig.MINI_MAP_SIZE, PlayerConfig.MINI_MAP_SIZE)
        self.threat_analysis = ThreatAnalysis(self.mini_map_grid, {UnitType.A2A: 80000})

        self.rule_player = RulePlayer(self.side)
        self.__agent_stat = NJ01Stat()

    def transform_action2command(self, action, raw_obs):
        """
        :param action:
        :param raw_obs:
        :return: (command, valid_action for MultipleHeadsAction)
        """
        cmds = []
        cmds.extend(self.rule_player.step(raw_obs))
        command, valid_actions = self._make_commands(action, raw_obs)
        cmds.extend(command)
        # print('red cmd:', raw_obs['sim_time'])
        # print(command)
        return cmds, valid_actions

    def unit2team(self, raw_obs):
        unit_team_map = {}
        for unit in raw_obs[self.side]['units']:
            unit_team_map[unit['ID']] = unit['TMID']
        return unit_team_map
            

    def collect_features(self, raw_obs, env_step_info):
        """
        user-defined interface to collect feature values (including historic features), which will be
        transformed to state by o2s_transformer
        :param raw_obs: raw_obs from env
        :param env_step_info:
        :return: feature_template_values according to the feature_template_dict
            e.g., for feature_templates
            {
                "common_template": CommonFeatureTemplate(features={"last_action": OneHotFeature(depth=10)}),
                "entity_template": EntityFeatureTemplate(max_length=10, features={"pos_x": RangedFeature(limited_range=8)}),
                "spatial_template": SpatialFeatureTemplate(height=8, width=8, features={"visibility": PlainFeature()})
            }, it should return something like
            {
                "common_template": {"last_action": 5},
                "entity_template": {"pos_x": 6.6},
                "spatial_template": {"visibility": [[1] * 8] * 8}
            }
        """
        # print("\rcurr_time_test: {}".format(raw_obs['sim_time']), end='  ')
        print("current time: ", raw_obs['sim_time'])
        feature_template_values = self._make_feature_values(raw_obs)
        return feature_template_values

    def _make_feature_values(self, raw_obs):
        """根据场上所有可见unit的信息提取state vector"""
        my_units, self.my_unit_ids, my_units_masks = self._get_my_units_feature_values(raw_obs)
        en_units, self.en_unit_ids = self._get_en_units_feature_values(raw_obs)
        common = self._get_common_feature_values(raw_obs)
        mini_map = self._get_spatial_feature_values(raw_obs)

        feature_value = {}
        feature_value['my_units'] = my_units
        feature_value['en_units'] = en_units
        feature_value['mini_map'] = mini_map
        feature_value['common'] = common

        feature_value['selected_units_mask'] = {'mask': my_units_masks}
        return feature_value

    def _get_my_units_feature_values(self, raw_obs):
        my_units = []
        my_unit_ids = []
        masks = []
        for unit in raw_obs[self.side]['units']:
            if unit['LX'] in PlayerConfig.MY_UNIT_TYPES:
                my_unit_ids.append(unit['ID'])
                if unit['LX'] in PlayerConfig.MY_UNIT_MASK_TYPES:
                    masks.append(1)
                else:
                    masks.append(1)
                my_unit_map = {}
                my_unit_map['x'] = unit['X']
                my_unit_map['y'] = unit['Y']
                my_unit_map['z'] = unit['Z']
                my_unit_map['a2a'] = get_weapon_num(unit, MissileType.A2A)
                my_unit_map['a2g'] = get_weapon_num(unit, MissileType.A2G)
                my_unit_map['course'] = unit['HX']
                my_unit_map['speed'] = unit['SP']
                my_unit_map['locked'] = unit['Locked']
                my_unit_map['type'] = PlayerConfig.MY_UNIT_TYPES.index(unit['LX'])
                my_units.append(my_unit_map)
        mask_paddings = [0 for _ in range(PlayerConfig.MAX_MY_UNIT_LEN - len(masks))]
        masks.extend(mask_paddings)
        # TODO(zhoufan): masks长度可能超过60，会出问题吗？
        return my_units, my_unit_ids, masks[:PlayerConfig.MAX_MY_UNIT_LEN]

    def _get_en_units_feature_values(self, raw_obs):
        en_units = []
        en_unit_ids = []
        for unit in raw_obs[self.side]['qb']:
            if unit['LX'] in PlayerConfig.EN_UNIT_TYPES:
                en_unit_map = {}
                en_unit_map['x'] = unit['X']
                en_unit_map['y'] = unit['Y']
                en_unit_map['z'] = unit['Z']
                en_unit_map['course'] = unit['HX']
                en_unit_map['speed'] = unit['SP']
                en_unit_map['type'] = PlayerConfig.EN_UNIT_TYPES.index(unit['LX'])
                en_units.append(en_unit_map)
                en_unit_ids.append(unit['ID'])
        return en_units, en_unit_ids
    
    def _get_spatial_feature_values(self, raw_obs):
        mini_map = {}
        mini_map['my_a2a'] = self._get_binary_matrix(raw_obs, UnitType.A2A)
        mini_map['my_a2g'] = self._get_binary_matrix(raw_obs, UnitType.A2G)
        mini_map['en_a2a'] = self._get_binary_matrix(raw_obs, UnitType.A2A, True)
        mini_map['en_a2g'] = self._get_binary_matrix(raw_obs, UnitType.A2G, True)
        mini_map['threat'] = self.threat_analysis.get_threat_matrix(raw_obs['red'])
        return mini_map

    def _get_binary_matrix(self, raw_obs, type_, qb=False):
        binary_matrix = np.zeros((PlayerConfig.MINI_MAP_SIZE, PlayerConfig.MINI_MAP_SIZE))
        category = 'qb' if qb else 'units'
        for unit in raw_obs[self.side][category]:
            if unit['LX'] == type_:
                x_idx, y_idx = self.map_grid.get_idx(unit['X'], unit['Y'])
                binary_matrix[y_idx][x_idx] = 1 
        return binary_matrix

    def _get_common_feature_values(self, raw_obs):
        common_map = {}
        common_map['sim_time'] = raw_obs['sim_time']
        return common_map

    def _make_commands(self, actions, raw_obs):
        # 初始化 valid_action 
        selected_units = []
        for idx in actions['selected_units']:
            if idx > 0:
                selected_units.append(self.my_unit_ids[idx - 1])

        valid_actions = {}
        for key, value in actions.items():
            valid_actions[key] = 0.0 

        if len(selected_units) != 0:
            valid_actions['selected_units'] = 1.0
        else:
            valid_actions['selected_units'] = 0
        valid_actions['meta_action'] = 1.0

        commands = [unit for unit in raw_obs['red']['qb'] if unit['LX'] == UnitType.COMMAND]
        commands_pos = []
        for cmd in commands:
            commands_pos.append([cmd['X'], cmd['Y'], cmd['Z']])

        ships = [unit for unit in raw_obs['red']['qb'] if unit['LX'] == UnitType.SHIP]

        action_cmds = []
        meta_action = actions['meta_action']

        team_unit_map = self.unit2team(raw_obs)
        
        if meta_action == 0:    # 区域巡逻
            valid_actions['pos_x'] = 1.0
            valid_actions['pos_y'] = 1.0
            patrol_zone_x_idx = float(actions['pos_x'])
            patrol_zone_y_idx = float(actions['pos_y'])
            center_points = patrol_point_map[(patrol_zone_x_idx, patrol_zone_y_idx)]

            # center_point_x, center_point_y = self.map_grid.get_center(
            #     patrol_zone_x_idx, patrol_zone_y_idx)
            # center_points.append(center_point_x)
            # center_points.append(center_point_y)
            # center_points.append(8000)
            teams = set()
            for id_ in selected_units:
                if id_ in team_unit_map.keys():
                    teams.add(team_unit_map[id_])

            for team in teams:
                action_cmds.append(Command.area_patrol(team, center_points))
                # action_cmds.append(Command.area_patrol(id_, center_points))

        elif meta_action == 1:      # 空中拦截，目前认为 'target_unit' 只作为歼击机的拦截目标 
            valid_actions['target_unit'] = 1.0  # target unit valid 
            target_idx = actions['target_unit'] 
            # 给出了在 self.en_unit_ids 中的索引，从 self.en_unit_ids 中取出平台编号
            if len(self.en_unit_ids) > 0:
                for id_ in selected_units:
                    our_unit = self._id2unit(raw_obs, id_, is_enermy=False)
                    en_unit = self._id2unit(raw_obs, self.en_unit_ids[target_idx], is_enermy=True)
                    if our_unit and en_unit:
                        if self.cal_unit_dis(our_unit, en_unit) < 200.:       # 200 km 之内开始目标打击
                            action_cmds.append(Command.a2a_attack(id_, self.en_unit_ids[target_idx]))
                        else:
                            patrol_point = [en_unit["X"], en_unit["Y"], en_unit["Z"]]
                            for id_ in selected_units:
                                action_cmds.append(Command.area_patrol(id_, patrol_point))

        elif meta_action == 2:      # 随机朝着两个指挥所之一移动
            for id_ in selected_units:  # selected_units 选的都是我方单位
                random_ind = np.random.randint(0, len(commands_pos))
                target_point = commands_pos[random_ind]
                action_cmds.append(Command.area_patrol(id_, target_point))
            
        elif meta_action == 3:      # 对指挥所突击
            # valid_actions['target_unit'] = 1.0
            # target_idx = actions['target_unit']
            fire_range = 100
            direction = 270
            if len(self.en_unit_ids) > 0:
                for id_ in selected_units:
                    unit = self._id2unit(raw_obs, id_, is_enermy=False)
                    if unit['LX'] == UnitType.A2G:
                        for commander in commands:
                            # 距离指挥所 150 km 开启目标突击
                            if self.cal_unit_dis(unit, commander) < 150.:
                                print("attack command start !")
                                action_cmds.append(Command.target_hunt(id_, commander['ID'], fire_range, direction))
                                break

        else:       # 对船突击
            # valid_actions['target_unit'] = 1.0
            # target_idx = actions['target_unit']
            fire_range = 100
            direction = 270 
            if len(self.en_unit_ids) > 0:
                for id_ in selected_units:
                    unit = self._id2unit(raw_obs, id_, is_enermy=False)
                    if unit['LX'] == UnitType.A2G:
                        for ship in ships:
                            # 在 150 km 处开始发弹
                            if self.cal_unit_dis(unit, ship) < 150.:
                                print("attack ship start !")
                                action_cmds.append(Command.target_hunt(id_, ship['ID'], fire_range, direction))
                                break

        return action_cmds, valid_actions

    # 计算两个单位之间的距离
    def cal_unit_dis(self, unit1, unit2):
        return math.sqrt(math.pow(unit1["X"] - unit2["X"], 2) + math.pow(unit1["Y"] - unit2["Y"], 2))/1000.

    def _id2unit(self, raw_obs, id_val, is_enermy):
        if is_enermy:
            for unit in raw_obs[self.side]["qb"]:
                if unit["ID"] == id_val:
                    return unit
        else:
            for unit in raw_obs[self.side]["units"]:
                if unit["ID"] == id_val:
                    return unit 
        return None 
    
    def calculate_reward(self, raw_obs, env_step_info):
        """
        user-defined class to calculate reward for agent player
        :param raw_obs: raw_obs from env
        :param env_step_info:
        :return: reward
        """
        reward = self.reward_obj.get(raw_obs)
        print(reward)
        return reward

    def reset(self, raw_obs, env_step_info):
        super().reset(raw_obs, env_step_info)
        self.__init_variables()


# 给一个新版本，如果靠近指挥所能得到奖励，考虑飞机群相对于指挥所的大格子的位置，处于划分的16个格子的哪个位置，找到飞机相对于哪一个格子更近
# 以格子中心为圆心，50 km 为半径，就被划分到这一个里面，引导其往指挥所飞，但需要做好单位损失和之间的权重分配，又是一个调参的东西，需要测试。

class RedReward(object):

    def __init__(self):
        self.last_a2a_num = -1
        self.last_a2g_num = -1
        self.last_awacs_num = -1
        self.last_en_a2a_num = -1
        self.last_en_a2g_num = -1
        self.last_en_ship_num = -1
        self.last_en_command_num = -1

        self.comm_rew_flag = False
        self._mini_count = {}

    def _get_type_num_diff(self, raw_obs, side, type_, last_num):
        curr_num = get_type_num(raw_obs[side], [type_], consider_airport=True)
        diff = curr_num - last_num if last_num != -1 else 0
        return diff, curr_num

    # 找到 unit 距离哪个小格子比较近，返回的是格子编号，也就是 patrol_point_map 中的某一 key 值
    def find_grid_belong(self, unit):
        min_dis = 1e12
        ans = None
        for xypair, point in patrol_point_map.items():
            dis = self.cal_dis(point, (unit['X'], unit['Y'], unit['Z']))
            if dis < min_dis:
                ans = xypair
                min_dis = dis
        return ans 

    # 统计每一个格子里面的A2A和A2G的数量
    def get_mini_count(self, raw_obs):
        # 给每个格子里的飞机计数，返回一个 xypair -> num 的映射。即便空中单位全没有了，mini_count 也不会为空。
        mini_count = {xypair : 0 for xypair in patrol_point_map.keys()}
        for unit in raw_obs['red']['units']:
            if unit['LX'] == UnitType.A2A or UnitType.A2G:
                pos = self.find_grid_belong(unit)
                mini_count[pos] += 1
        return mini_count 
    
    def compare_mini_count(self, mini_count1, mini_count2):
        # mini_count2 为现在的小区域飞机分布，mini_count1 为上一步的小区域飞机分布
        area_name = ['area1', 'area2', 'area3']
        area_type_count1 = {num : 0 for num in area_name}
        area_type_count2 = {num : 0 for num in area_name}

        for pos, num in mini_count1.items():
            if pos in patrol_area_type1:
                area_type_count1['area1'] += num
            if pos in patrol_area_type2:
                area_type_count1['area2'] += num
            if pos in patrol_area_type3:
                area_type_count1['area3'] += num

        for pos, num in mini_count2.items():
            if pos in patrol_area_type1:
                area_type_count2['area1'] += num
            if pos in patrol_area_type2:
                area_type_count2['area2'] += num
            if pos in patrol_area_type3:
                area_type_count2['area3'] += num

        print("area_type_count1", area_type_count1)
        print("area_type_count2", area_type_count2)

        sum_rew = 0
        sum_rew += 1.0 * (area_type_count2['area1'] - area_type_count1['area1'])    # 鼓励进入区域 1 和区域 2
        sum_rew += area_type_count2['area2'] - area_type_count1['area2']
        sum_rew += -(area_type_count2['area3'] - area_type_count1['area3']) # 鼓励逃离区域 3

        if sum_rew > 0:
            return 2
        if sum_rew == 0:
            return 0
        if sum_rew < 0:
            return -2
        
                
    # 得到接近指挥所的回报
    def appro_comm_rew(self, raw_obs):
        rew = 0
        if self.comm_rew_flag:
            if raw_obs['sim_time'] > 600:
                curr_mini_count = self.get_mini_count(raw_obs)
                rew += self.compare_mini_count(self._mini_count, curr_mini_count)
                self._mini_count = curr_mini_count
        else:
            self.comm_rew_flag = True
            self._mini_count = self.get_mini_count(raw_obs)
        
        return rew
        

    # unit1 和 unit2 为列表或是元组
    def cal_dis(self, unit1, unit2):
        return math.sqrt(math.pow(unit1[0] - unit2[0], 2) + math.pow(unit1[1] - unit2[1], 2))/1000.


    def get(self, raw_obs):
        my_a2a_num_diff, my_a2a_num = self._get_type_num_diff(
                        raw_obs, 'red', UnitType.A2A, self.last_a2a_num)
        self.last_a2a_num = my_a2a_num
        # print('a2a ', my_a2a_num, self.last_a2a_num, my_a2a_num_diff)

        my_a2g_num_diff, my_a2g_num = self._get_type_num_diff(
            raw_obs, 'red', UnitType.A2G, self.last_a2g_num)
        self.last_a2g_num = my_a2g_num
        
        my_awacs_num_diff, my_awacs_num = self._get_type_num_diff(
            raw_obs, 'red', UnitType.AWACS, self.last_awacs_num)
        self.last_awacs_num = my_awacs_num

        en_a2a_num_diff, en_a2a_num = self._get_type_num_diff(
            raw_obs, 'blue', UnitType.A2A, self.last_en_a2a_num)
        self.last_en_a2a_num = en_a2a_num
        

        en_a2g_num_diff, en_a2g_num = self._get_type_num_diff(
            raw_obs, 'blue', UnitType.A2G, self.last_en_a2g_num)
        self.last_en_a2g_num = en_a2g_num

        en_ship_num_diff, en_ship_num = self._get_type_num_diff(
            raw_obs, 'blue', UnitType.SHIP, self.last_en_ship_num)
        self.last_en_ship_num = en_ship_num

        en_command_num_diff, en_command_num = self._get_type_num_diff(
            raw_obs, 'blue', UnitType.COMMAND, self.last_en_command_num)
        self.last_en_command_num = en_command_num


        if raw_obs['sim_time'] > 7992. and en_command_num == 2:
            win_lose_penalize = -10
        else:
            win_lose_penalize = 0

        control_comm_rew = self.appro_comm_rew(raw_obs)
        print("value of control_comm_rew", control_comm_rew)

        rew = my_a2a_num_diff * 0.5 + my_a2g_num_diff * 1 + en_a2a_num_diff * (-1) + en_a2g_num_diff * (-1) + \
            en_ship_num_diff * (-5) + en_command_num_diff * (-8) + my_awacs_num_diff * 5 + win_lose_penalize + control_comm_rew 

        return rew