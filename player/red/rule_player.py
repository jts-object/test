
from env.env_def import UnitType, RED_AIRPORT_ID, MapInfo
from common.cmd import Command
from common.grid import MapGrid
from common.interface.base_rule import BaseRulePlayer
from common.interface.task import Task, TaskState
from common.units import Unit, A2G
from common.threat_analysis import ThreatAnalysis
from env.env_util import azimuth_angle
import numpy as np

A2A_PATROL_PARAMS = [270, 10000, 10000, 250, 7200]


class A2GTask(Task):
    def __init__(self, max_unit_num, fire_range, start_time, map_grid, task_state=TaskState.PREPARE):
        super().__init__(task_state)
        self.max_unit_num = max_unit_num
        self.fire_range = fire_range
        self.target_unit = None
        self.start_time = start_time
        self.map_grid = map_grid
    
    def update_task_state(self, obs):
        pass

    def get_all_missiles(self):
        missile_num = 0
        for _, unit in self.units_map.items():
            missile_num += unit.get_missile_num() 
        return missile_num

    def update(self, alive_unit_map, obs):
        self.update_units_map(alive_unit_map)
        self.update_task_state(obs)

    def run(self, idle_unit_map, threat_matrix):
        cmds = []
        if self.task_state == TaskState.START: 
            while len(self.units_map) < self.max_unit_num or self.get_all_missiles() < self.max_unit_num * 2: 
                if len(idle_unit_map) == 0:
                    break
                unit = idle_unit_map.popitem()[1]
                self.add_unit(unit)
            for _, unit in self.units_map.items():
                if unit.get_missile_num() != 0:
                    cmd_list = self.astar_task_run(unit, threat_matrix)
                    cmds.extend(cmd_list)
                else:
                    cmds.append(Command.return2base(unit.id, RED_AIRPORT_ID))
        elif self.task_state == TaskState.FINISH:
            for unit_id, unit in self.units_map.items():
                if unit.get_missile_num() != 0: 
                    cmds.append(Command.return2base(unit_id, RED_AIRPORT_ID))
            self.finish()
        return cmds
 
    def astar_task_run(self, unit, threat_matrix):
        cmds = []

        end_point_x, end_point_y = self.map_grid.get_idx(self.target_unit.x, self.target_unit.y)
        end_point = [end_point_x, end_point_y]
        path = unit.astar_path_finding(threat_matrix, end_point, self.map_grid)
        # 找到路径
        if len(path) > 1:
            x, y = self.map_grid.get_center(path[1][0], path[1][1])
            cmds.append(Command.area_patrol(unit.id, [x, y, 8000], A2A_PATROL_PARAMS))
    
        if unit.compute_2d_distance_unit(self.target_unit) < 100000:
            direction = azimuth_angle(unit.x, unit.y, self.target_unit.x, self.target_unit.y)
            cmds.append(Command.target_hunt(unit.id, self.target_unit.id, fire_range=100, direction=direction))

        return cmds
    

class OneForAllA2gTask(A2GTask):
    def __init__(self, max_unit_num, fire_range, start_time, map_grid):
        super().__init__(max_unit_num, fire_range, start_time, map_grid)
    
    def update_task_state(self, raw_obs):
        self.target_unit = None
        for unit in raw_obs['red']['qb']:
            if unit['LX'] == UnitType.SHIP:
                self.target_unit = Unit(unit)
                break
            if unit['LX'] == UnitType.COMMAND:
                if unit['Y'] > 0:
                    self.target_unit = Unit(unit)
                    break
                else:
                    self.target_unit = Unit(unit)
    
        if self.target_unit is None:
            self.task_state = TaskState.FINISH
        else:
            if raw_obs['sim_time'] > self.start_time:
                self.task_state = TaskState.START


class RulePlayer(BaseRulePlayer):

    def __init__(self, side):
        super().__init__(side)
        self.map_grid = MapGrid(
            (MapInfo.X_MIN, MapInfo.Y_MAX), (MapInfo.X_MAX, MapInfo.Y_MIN), 20, 20)
        self.a2g_task = OneForAllA2gTask(6, 100, 500, self.map_grid)
        # A*算法时，只考虑了歼击机的威胁，没有考虑舰船和地防的威胁（有可能无法避免）
        self.threat_analysis = ThreatAnalysis(self.map_grid, {UnitType.A2A: 100000})

    def _take_off(self, raw_obs):
        cmds = []
        fly_types = [UnitType.A2A, UnitType.A2G, UnitType.JAM]
        for type_ in fly_types:
            num = min(self._get_waiting_aircraft_num(raw_obs, type_), 4)
            # if self._get_waiting_aircraft_num(raw_obs, type_):
            if num:
                cmds.append(Command.takeoff_areapatrol(RED_AIRPORT_ID, num, type_))

        return cmds

    def _awacs_task(self, raw_obs):
        cmds = []
        patrol_points = [50000, 0, 8000]
        # TODO(zhoufan): 是否应该将awacs的id缓存起来
        for unit in raw_obs[self.side]['units']:
            if unit['LX'] == UnitType.AWACS:
                cmds.append(
                    Command.awacs_areapatrol(
                        unit['ID'], patrol_points))
                break
        return cmds
    
    def _get_units_map(self, raw_obs, type_):
        units_map = {}
        for info_map in raw_obs[self.side]['units']:
            if info_map['LX'] == type_:
                units_map[info_map['ID']] = A2G(info_map)
        return units_map

    def step(self, raw_obs):
        cmds = []
        cmds.extend(self._take_off(raw_obs))
        # cmds.extend(self._awacs_task(raw_obs))

        a2g_map = self._get_units_map(raw_obs, UnitType.A2G)
        self.a2g_task.update(a2g_map, raw_obs)
        for unit_id, _ in self.a2g_task.get_units_map().items():
            a2g_map.pop(unit_id)
        threat_matrix = self.threat_analysis.get_threat_matrix(raw_obs[self.side])
        # print(threat_matrix)
        # threat_matrix = np.ones((self.map_grid.x_n, self.map_grid.y_n))
        # cmds.extend(self.a2g_task.run(a2g_map, threat_matrix))
        # patrol_cmds = [cmd for cmd in cmds if 'patrol'in cmd['maintype']]
        # print('total, patrol, else', len(cmds), len(patrol_cmds), len(cmds)-len(patrol_cmds))
        return cmds
